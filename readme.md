Dag 2 CSRF_XSRF, Kryptering og biblioteker

- crypt.py
- test.py
- socket1.py
- crypt_socket1.py

Dag 4, tirsdag Race Conditions II, Fejl Håndtering

- url.py

Dag 5 fejlhåndtering

- evt.evtx
- main.py

Dag 6 input validering

- aaaaabcccd.py
- aaabccd.py
- xml.txt
- xsd.txt

En liste over de exceptions man kan forvente fra metoderne i 
cryptography:

https://cryptography.io/en/latest/exceptions/

Writing to Windows Event log using win32evtlog from pywin32 library:

https://stackoverflow.com/questions/51385195/writing-to-windows-event-log-using-win32evtlog-from-pywin32-library
