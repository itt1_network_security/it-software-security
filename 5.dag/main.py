import Evtx.Evtx as evtx

def open_evtx(input_file):
        """Opens a Windows Event Log and displays common log parameters.

        Arguments:
            input_file (str): Path to evtx file to open
        """

        with evtx.Evtx(input_file) as open_log:
            header = open_log.get_file_header()

            for chunk in open_log.chunks():
                        for record in chunk.records():
                            print( record.xml() );


if __name__ == '__main__':
    open_evtx("evt.evtx")