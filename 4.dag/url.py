# Exception Python
import urllib
import urllib.request
import sys

try:
    url = 'https://students.dk/'
    req = urllib.request.Request(url)
    f = urllib.request.urlopen(req)
    content = f.read()
    print(content)

except urllib.error.URLError as e:
    print(int(1/0))
    print("Error Code: ", e)
finally:
    print("exit")
    sys.exit(0)
