import re

def jaellernej(s): 
  k = re.match('a+b?c{2,4}d',s)
  if k:
    return 'ja'
  else:
    return 'nej'

print("De skal give ja:")
print(jaellernej("aaabcccd"))
print(jaellernej("aaacccd"))
print(jaellernej("abcccd"))
print(jaellernej("aaabccd"))
print(jaellernej("aaabccccd"))
print(jaellernej("aaaccccd"))

print()
print("De skal give nej:")
print(jaellernej("bcccd"))
print(jaellernej("cccd"))
print(jaellernej("aaabcd"))
print(jaellernej("aaacd"))
print(jaellernej("abcccccd"))
print(jaellernej("acccccd"))
print(jaellernej("aaaccccx"))

 
