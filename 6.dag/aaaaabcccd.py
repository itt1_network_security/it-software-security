

def jaellernej(s):
  i = 0 
  # Den skal begynde med et a
  if s[i]!='a':
    return 'nej'
  i += 1
  # Saa maa der komme vilkaarligt mange a-er 
  while s[i]=='a':
    i += 1
  # Nu kan der være 0 eller 1 forekomster af b
  if s[i]=='b':
    i += 1
  # Der skal være mindst 2 forekomster af c
  if s[i]!='c':
    return 'nej'
  i += 1
  if s[i]!='c':
    return 'nej'
  i += 1
  # Der må nu være fra 0 til 2 forekomster af c
  c_count = 0
  while s[i]=='c':
    i += 1
    c_count += 1
  if c_count>2:
    return 'nej'
  # Der skal nu komme et d
  if s[i]!='d':
    return 'nej'

  return 'ja'

print("De skal give ja:")
print(jaellernej("aaabcccd"))
print(jaellernej("aaacccd"))
print(jaellernej("abcccd"))
print(jaellernej("aaabccd"))
print(jaellernej("aaabccccd"))
print(jaellernej("aaaccccd"))

print()
print("De skal give nej:")
print(jaellernej("bcccd"))
print(jaellernej("cccd"))
print(jaellernej("aaabcd"))
print(jaellernej("aaacd"))
print(jaellernej("abcccccd"))
print(jaellernej("acccccd"))
print(jaellernej("aaaccccx"))

 
