from cryptography.fernet import Fernet

key = Fernet.generate_key()
f = Fernet(key)
ciphertext = f.encrypt(b"A really secret message.")
print(ciphertext)
cleartext = f.decrypt(ciphertext)
print(cleartext)
