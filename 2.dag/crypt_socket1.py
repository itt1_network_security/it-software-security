from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
import binascii
import json
import websocket


backend = default_backend()

# choose encryption algorithm and key

key = binascii.unhexlify("82828282828282828282828282828282")
cipher = Cipher(algorithms.AES(key), modes.ECB(), backend=backend)


# from a string to a ciphertext in hex (with known key)

cleartext = "a secret message"      # string exactly 16 bytes
cleartextbinary = cleartext.encode("utf-8")    # make it into a byte-array
encryptor = cipher.encryptor()
ct = encryptor.update(cleartextbinary) + encryptor.finalize()
cthex = binascii.hexlify(ct)
print(cthex)  # ciphertext in hex is printed

# from a ciphertext in hex to a string (with known key)

ct2 = binascii.unhexlify(cthex)
decryptor = cipher.decryptor()
cleartextbinary2 = decryptor.update(ct2) + decryptor.finalize()
cleartext2 = cleartextbinary2.decode('utf-8')   # from byte.array to string
print(cleartext2)

ws = websocket.WebSocket()
ws.connect('ws://chat.students.dk:8080/')

while True:
 message = ws.recv()

 if (len(message)==0):
   print("Connection closed by server\n")
   break

 print(message)
 j = json.loads(message)
 hex = j['message'].replace(" ","")
 print(hex)
